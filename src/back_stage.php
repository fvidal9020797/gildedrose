<?php
/**
 * Created by PhpStorm.
 * User: Luis Fernando Vidal
 * Date: 15/6/2019
 * Time: 05:10 AM
 */

class BackStage  implements Products
{
    private $item;

    function calculate($item)
    {
        $this->item = $item;
        self::decreaseSellByDayValueByOne();
        if (self::sellByDayValueIsOver(10)) {
            self::increaseQualityBy(1);
        } else if (self::sellByDayValueIsOver(5)) {
            self::increaseQualityBy(2);
        } else if (self::sellByDayValueIsOver(0)) {
            self::increaseQualityBy(3);
        } else {
            self::dropQualityToZero();
        }

    }

    private function decreaseSellByDayValueByOne()
    {
        $this->item->sell_in -= 1;
    }

    private function sellByDayValueIsOver($dayNumber)
    {
        return $this->item->sell_in > $dayNumber;
    }

    private function increaseQualityBy($qualityValue)
    {
        $this->item->quality += $qualityValue;
    }

    private function dropQualityToZero()
    {
        $this->item->quality = 0;
    }
}