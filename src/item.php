<?php
/**
 * Created by PhpStorm.
 * User: Luis Fernando Vidal
 * Date: 15/6/2019
 * Time: 05:12 AM
 */

class Item
{
    public $name;

    // cantidad de dias para vender el articulo
    //
    public $sell_in;

    //valor de calidad del articulo
    // la calidad nunca es negatica
    // se degrada dos veces mas rapido si la cantidad de dias es 0
    //Aged Brie aumenta la calidad cuanto mayor sea
    //La calidad de un articulo nunca es mas de 50
    //Sulfuras Nunca tine que ser vendido o disminuye la calidad
    //Backstage Pass- aumenta su valor a medida que se acerca su valor SellIn
    //La calidad aumenta en 2 cuando hay 10 dias o menos y en 5 dias o menos
    //la calidad baja a 0 despues del concierto
    public $quality;

    function __construct($name, $sell_in, $quality)
    {
        $this->name = $name;
        $this->sell_in = $sell_in;
        $this->quality = $quality;
    }

    public function __toString()
    {
        return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }
}

