<?php
/**
 * Created by PhpStorm.
 * User: Luis Fernando Vidal
 * Date: 15/6/2019
 * Time: 05:04 AM
 */

class OtherItem implements Products
{
    private $item;


    function calculate($item)
    {
        $this->item = $item;
        self::decreaseSellByDayValueByOne();
        if (self::sellByDayValueIsOverZero()) {
            self::decreaseQualityBy(self::decreasingValueOverZeroDaysToSell());
        } else {
            self::decreaseQualityBy(self::decreasingValueForZeroOrLessDaysToSell());
        }
    }

    private function decreaseSellByDayValueByOne()
    {
        $this->item->sell_in -= 1;
    }

    private function sellByDayValueIsOverZero()
    {
        return $this->item->sell_in > 0;
    }

    private function decreaseQualityBy($qualityValue)
    {
        $this->item->quality -= $qualityValue;
    }

    public function decreasingValueOverZeroDaysToSell()
    {
        return 1;
    }

    private function decreasingValueForZeroOrLessDaysToSell()
    {
        return self::decreasingValueOverZeroDaysToSell() * 2;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

}