<?php
include_once ('manager_products.php');
include_once ('products.php');
include_once ('otherItem.php');
include_once ('age_brie.php');
include_once ('back_stage.php');
include_once ('item.php');
include_once ('sulfuras.php');
include_once ('Conjured.php');
include_once ('age_brie.php');

class GildedRose
{
    private $items;

    function __construct($items)
    {
        $this->items = $items;
    }

    function update_quality()
    {
        foreach ($this->items as $item) {
            switch ($item->name) {
                case 'Sulfuras, Hand of Ragnaros':
                    $managerProduct = new ManagerProducts(new Sulfuras());
                    $managerProduct->cal($item);
                    break;
                case 'Elixir of the Mongoose':
                    $managerProduct = new ManagerProducts(new OtherItem());
                    $managerProduct->cal($item);
                    break;
                case '+5 Dexterity Vest':
                    $managerProduct = new ManagerProducts(new OtherItem());
                    $managerProduct->cal($item);
                    break;
                case 'Backstage passes to a TAFKAL80ETC concert':
                    $managerProduct = new ManagerProducts(new BackStage());
                    $managerProduct->cal($item);
                    break;
                case 'Conjured Mana Cake':
                    $managerProduct = new ManagerProducts(new Conjured());
                    $managerProduct->cal($item);
                    break;
                case 'Aged Brie':
                    $managerProduct = new ManagerProducts(new AgedBrie());
                    $managerProduct->cal($item);
                    break;
            }
        }
    }
}

