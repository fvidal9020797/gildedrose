<?php
/**
 * Created by PhpStorm.
 * User: Luis Fernando Vidal
 * Date: 15/6/2019
 * Time: 05:06 AM
 */
include_once ('products.php');
class AgedBrie implements Products
{
    private $item;

    function calculate($item)
    {
        $this->item = $item;
        self::decreaseSellByDayValueByOne();
        self::increaseQualityByOne();
    }

    private function decreaseSellByDayValueByOne()
    {
        $this->item->sell_in -= 1;
    }

    private function increaseQualityByOne()
    {
        $this->item->quality += 1;
    }
}