<?php
/**
 * Created by PhpStorm.
 * User: Luis Fernando Vidal
 * Date: 15/6/2019
 * Time: 05:02 AM
 */

interface Products
{
    function calculate($item);
}