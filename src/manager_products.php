<?php
/**
 * Created by PhpStorm.
 * User: Luis Fernando Vidal
 * Date: 15/6/2019
 * Time: 04:59 AM
 */

class ManagerProducts
{
    private static $lowCalityProduct = 0;
    private static $hightCalityProduct = 50;
    private $products;

    /**
     * ManagerProducts constructor.
     * @param $products
     */
    public function __construct($products)
    {
        $this->products = $products;
    }

    public function cal($item)
    {
        $this->products->calculate($item);
        self::custom($item);
    }

    private function custom($item)
    {
        if ($item->quality < 0) $item->quality = self::$lowCalityProduct;
        if ($item->quality > 50) $item->quality = self::$hightCalityProduct;
    }

}